var $dom = {},
		validationOptions = {
			formClass: 'validate',
			beforeSubmit: function($form){
				$form.find('input.disabled').prop('disabled', true);
				$form.find('a.submit').addClass('disabled');            
			},
			afterValidation: function($form){
				$form.find('input.disabled').prop('disabled', false);
				$form.find('a.submit').removeClass('disabled');           
			}
		};

function cacheDOM(){
	$dom = {
		document: $(document),
		window: $(window),
		html: $('html'),
		body: $('body'),
		header: $('#page > header'),
		main: $('#main'),
		categories: $('#categories'),
		footer: $('#page > footer')
	};
}

function isPhone(){
	if($dom.body.hasClass('phone')) return true;
	return false;
}

(function($) {
	$.fn.sideMenu = function(o) {
		o = $.extend(true, {
			speed: 150,
			delay: 3000,
			easing: 'easeOutExpo'
		}, o);
		return this.each(function() {
			var $root;

			$root = $(this);
			$('.li-search', $root).sideMenuSearch();
			return $('.li-share', $root).sideMenuShare();
		});
	};
	$.fn.sideMenuSearch = function(o) {
		o = $.extend(true, {
			speed: 150,
			delay: 3000,
			easing: 'easeOutExpo'
		}, o);
		return this.each(function() {
			var $input, $item, $items, $link, $root, $search, hide, init, onBlur, onFocus, show, t;

			$root = $(this).closest('.side-menu');
			$items = $('.li', $root);
			$item = $(this);
			$link = $('.link', $item);
			$search = $('.search', $item);
			$input = $('input', $search);
			t = null;
			init = function() {
				$search.on('focusin', onFocus);
				$search.on('focusout', onBlur);
				$link.click(show);
				$item.on('hide', hide);
				$(document).click(hide);
				return $root.click(function(e) {
					return e.stopPropagation();
				});
			};
			onFocus = function() {
				return clearTimeout(t);
			};
			onBlur = function() {
				return t = setTimeout(hide, o.delay);
			};
			show = function() {
				clearTimeout(t);
				$items.not($item).trigger('hide');
				$item.addClass('hover');
				$search.stop(true, true).animate({
					'width': 240
				}, {
					duration: o.speed,
					easing: o.easing,
					queue: false
				});
				return $input.focus();
			};
			hide = function() {
				clearTimeout(t);
				return $search.stop(true, true).animate({
					'width': 0
				}, {
					duration: o.speed,
					easing: o.easing,
					queue: false,
					complete: function() {
						return $item.removeClass('hover');
					}
				});
			};
			return init();
		});
	};
	return $.fn.sideMenuShare = function(o) {
		o = $.extend(true, {
			speed: 150,
			delay: 3000,
			easing: 'easeOutExpo'
		}, o);
		return this.each(function() {
			var $item, $items, $link, $root, $share, hide, init, initFacebook, initTwitter, initVKontakte, onOut, onOver, show, t;

			$root = $(this).closest('.side-menu');
			$items = $('.li', $root);
			$item = $(this);
			$link = $('.link', $item);
			$share = $('.share', $item);
			t = null;
			init = function() {
				$item.hover(onOver, onOut);
				$link.click(show);
				$item.on('hide', hide);
				$(document).click(hide);
				$root.click(function(e) {
					return e.stopPropagation();
				});
				//initFacebook();
				//initVKontakte();
				return initTwitter();
			};
			onOver = function() {
				return clearTimeout(t);
			};
			onOut = function() {
				return t = setTimeout(hide, o.delay);
			};
			show = function() {
				clearTimeout(t);
				$items.not($item).trigger('hide');
				$item.addClass('hover');
				return $share.stop(true, true).animate({
					'width': 180
				}, {
					duration: o.speed,
					easing: o.easing,
					queue: false
				});
			};
			hide = function() {
				clearTimeout(t);
				return $share.stop(true, true).animate({
					'width': 0
				}, {
					duration: o.speed,
					easing: o.easing,
					queue: false,
					complete: function() {
						return $item.removeClass('hover');
					}
				});
			};
			initFacebook = function() {
				var $el, url;

				$el = $('.share-facebook', $root);
				url = $el.data('url');
				return $el.click(function() {
					return FB.ui({
						link: url,
						method: 'feed'
					});
				});
			};
			initVKontakte = function() {
				var $el, button, text, url;

				$el = $('.share-vkontakte', $root);
				url = encodeURIComponent($el.data('url'));
				text = $el.html();
				button = VK.Share.button({
					url: url
				}, {
					text: text,
					type: 'custom'
				});
				return $el.html(button);
			};
			initTwitter = function() {
				var $el, height, left, opts, text, top, url, width;

				$el = $('.share-twitter', $root);
				url = encodeURIComponent($el.data('url'));
				text = encodeURIComponent($el.data('text'));
				width = 550;
				height = 450;
				left = (screen.width - width) / 2;
				top = (screen.height - height) / 2;
				url = 'http://twitter.com/share?url=' + url + '&text=' + text;
				opts = 'width=' + width + ',height=' + height + ',top=' + top + ',left=' + left + ',scrollbars=1,resizable=1';
				return $el.click(function() {
					return window.open(url, 'tweet', opts);
				});
			};
			return init();
		});
	};
})(jQuery);



function doServerValidate(o){
	var specialInputs = {
	};
	o = $.extend({
		formClass: 'validate',
		afterValidation: null,
		submitAction: null
	}, o);

	$.fn.setError = function(){
		return this.each(function(){
			var $el = $(this);
			if($el.parent('.ik_select').length){
				$el.parent('.ik_select').find('.ik_select_link').addClass('error');
			}
			$el.addClass('error').one('focus click', function(){
				$(this).removeError();
			});
		});
	}

	$.fn.removeError = function(){
		return this.each(function(){
			var $el = $(this);
			if($el.parent('.ik_select').length){
				$el.parent('.ik_select').find('.ik_select_link').removeClass('error');
			}
			$el.removeClass('error');
		});   
	}

	$dom.html.on('submit', 'form.'+o.formClass, function(e){
		var $form = $(e.target);
		$form.ajaxSubmit({
			data: {
				ajax: ajaxFormName = $form.attr('id') || $form.attr('name') || ''
			},
			beforeSubmit: o.beforeSubmit($form) || null,
			complete: function(response){
				var validSuccess = true;
				response = $.parseJSON(response.responseText);

				var len = 0;
				for(var key in response){
					len += Number(response.hasOwnProperty(key));
				}
				if(len > 0){
					validSuccess = false;
				}

				$form.find('input, select, textarea, .item').each(function(){
					var $el = $(this);
					if(response[$el.attr('id')]){
						$el.setError();
						validSuccess = false;
					} else {
						$el.removeError();
					}
				});
				$.each(response, function(index, value){
					if(!$('#'+index).length){
						if($(specialInputs[index]).length){
							$(specialInputs[index]).setError();
						}
					}
				});

				if(typeof o.afterValidation == 'function'){
					o.afterValidation($form);
				}

				if(validSuccess !== false){
					if(typeof o.submitAction == 'function'){
						o.submitAction.call($form);
						return false;
					} else {
						$form[0].submit();
					}
				} else {
					//errors
					if($('.error').length){
						$('html, body').scrollTop($('.error').eq(0).offset().top);
					}
					return false;
				}
			}
		});
		return false;
	});

}

$.fn.formElementAction = function(o){
	var o = $.extend({
		completeAction: null
	}, o);
	return this.each(function(){
		var $el = $(this),
				$form = $el.closest('form'),
				url = $el.data('url') || $form.attr('action'),
				type = $form.attr('method') || 'post';
		$form.ajaxSubmit({
			url: url,
			type: type,
			complete: function(data){
				if(typeof o.completeAction == 'function'){
					o.completeAction.call($el, data);
				}
			}
		});     
	});
}

function doForms(){
	$dom.html.on('submit', 'form:not(.noajax)', function(e){
		e.preventDefault();
		$form = $(e.target);
		$form.ajaxSubmit({
			complete: function(response){
				var data = $.parseJSON(response.responseText),
						error = data.error,
						status = data.status,
						redirect = data.redirect;
				if($form.hasClass('lazyload-form')){
					var html = data.html,
							last = data.last,
							$startInput = $form.find('.start-input'),
							$container = $form.closest('.lazyload');
							if(!$container.length) $container = $form.parent().prev();
					if(html){
						var $tmp = $('<div>' + html + '</div>'),
								appended = [];
						$tmp.children().each(function(){
							appended.push($(this).get(0));
						});
						$container.append(appended);
						if($container.hasClass('isotope')){
							$container.isotope('appended', appended);							
						}
						$dom.window.resize();
					}
					if(last){
						$form.closest('div').hide();
					} else {
						$startInput.val($container.find('.item').length);
					}
					return;
				}
				if($form.is('#comment-form')){
					var html = data.html,
							$container = $form.closest('.comments').find('.comments-list');
					if(html){
						$container.prepend(html);
						$dom.window.resize();
						$form[0].reset();
						return;
					}
				}
				if(status){
					$('.submit-status').remove();
					$form.after('<div class="submit-status text'+(error?' error':'')+'">' + status + '</div>');
				}
				if(!error && $form.is('#feedback-form')){
					$form[0].reset();
				}
				if(redirect){
					window.location.replace(redirect);
				}
			}
		});
	});

	$dom.html.on('click', 'form a.submit:not(.disabled)', function(e){
		e.preventDefault();
		var $a = $(this),
				$form = $a.closest('form');
		$form.submit();
	});

//	doServerValidate(validationOptions);
}

$.fn.ajaxShares = function(){
	return this.each(function(){
		var $root = $(this),
				$sharesCount = $root.find('.count');
		$root.bind('click', function(e){
			var $a = $(this),
					url = $a.data('url');
			if($a.hasClass('like')){
				e.preventDefault();
			}
			if(url){
				$.ajax({
					url: url,
					complete: function(data){
						if(data && data.responseText){
							try {
								data = $.parseJSON(data.responseText);
							} catch(e) {
								return;
							}
						} else return;
						if(data.count){
							$sharesCount.text(data.count);
						}
					}
				});
			}
		});
	});	
};

function resizeActions(){
	$dom.window.bind('resize', function(){
		$dom.main.css('min-height', '');
		var mp = 160,
				wh = $dom.window.height(),
				hh = $dom.header.outerHeight(),
				mh = $dom.main.outerHeight(),
				fh = $dom.footer.outerHeight();
		if(wh > hh + mh + fh){
			$dom.main.css('min-height', wh-fh-hh-mp);
		}

		if(isPhone() || 1){
			var $mc = $('body > .menu-container');
			if($mc.length < 2) return;
			var $i1 = $mc.eq(0).find('i').eq(0),
					$i2 = $mc.eq(1).find('i').eq(0),
					$b1 = $('header .l .topmenu .button'),
					$b2 = $('header .r .topmenu .button');
			$i1.css('left', $b1.position().left+12);
			$i2.css('left', $b2.position().left-8);
		}

	}).resize();
}



$(function(){
	cacheDOM();
	doForms();
	if(!isPhone()){
		$('.isotope').isotope({
			itemSelector: '.item',
			layoutMode: 'packery',
			stamp: '.stamp'
		});		
	}

	$('.topmenu').each(function(){
		var $root = $(this),
				$button = $root.find('a.button'),
				$menuContainer = $root.find('.menu-container');
		if(isPhone() && $menuContainer.length){
			var $menuContainer2 = $menuContainer.appendTo($dom.body).prepend('<i />');
			if($root.hasClass('profile')){
				$menuContainer2.addClass('profile').find('i').eq(0).addClass('r');				
			}
		}
		$button.bind('click', function(e){
			e.preventDefault();
			e.stopPropagation();
			if($menuContainer.is(':visible')){
				$menuContainer.hide();
			} else {
				$('.menu-container').hide();
				$menuContainer.show();
			}
		});			
		$(document).bind('click', function(){
			if($menuContainer.is(':visible')){
				$menuContainer.hide();
			}
		})
	});

	$('a.top').bind('click', function(e){
		e.preventDefault();
		$('html, body').stop().animate({scrollTop:0}, 400, 'easeInOutQuad');
	});

	$('.side-menu').sideMenu();

	$('.public .author-info').each(function(){
		if(isPhone) return false;
		var $blogItemHeader = $('.blog-item .item-header').eq(0);
		if(!$blogItemHeader.length) return;
		$(this).css('margin-top', $blogItemHeader.height()+80);
	});

	$('.blog-item .shares a, .blog-item .likes a').ajaxShares();

	resizeActions();
});
